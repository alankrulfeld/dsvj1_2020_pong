#include "raylib.h"
#include "credits.h"
#include "juego.h"
#include "controls.h"
#include "enum.h"
void Menu()
{
	// Initialization
		//--------------------------------------------------------------------------------------
	const int screenWidth  = 800;
	const int screenHeight = 500;

	InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");

	SetTargetFPS(60);               // Set our game to run at 60 frames-per-second
	
									//--------------------------------------------------------------------------------------

	// Main game loop
	int aux=0;
	MENU menu = (MENU)aux;
	while( !WindowShouldClose )
	{
		switch ( menu )
		{
		case MENU::GAME:
			Game( );
			break;
		case MENU::CONTROLS:
			Controls( );
			break;
		case MENU::CREDITS:
			Credits( );
			break;
		default:
			break;
		}
		BeginDrawing( );




		ClearBackground(BLACK);

		DrawText(TextFormat("PONG"), 50, 20, 20, LIGHTGRAY);



		EndDrawing( );
	}
	// De-Initialization
	//--------------------------------------------------------------------------------------
	CloseWindow( );        // Close window and OpenGL context
	//-----------------------------------------------------------------------------

}



